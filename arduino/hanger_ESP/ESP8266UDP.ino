// 自分のポート番号
char myIP[16];
#define MY_OSC_PORT 8091

// 送り先IPアドレスとポート番号
const IPAddress TARGET_IP(192, 168, 11, 4);
#define TARGET_PORT 8090

WiFiUDP Udp;
OSCErrorCode error;

 int smokeON = 1;


void oscSetup() {
  Udp.begin(MY_OSC_PORT);

  // 送信テスト(自分のIPアドレスを通知)
  {
    sprintf(myIP, "IP is %d.%d.%d.%d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3] );
    OSCMessage smsg("/ip");
    smsg.add(myIP);
    Udp.beginPacket(TARGET_IP, TARGET_PORT);
    smsg.send(Udp);
    Udp.endPacket();
    smsg.empty();
  }

  
}

//OSCデバッグ用。/HNGにきたものにgotit返す
void dispatchHNG(OSCMessage &msg) {
int ledState = msg.getInt(0);
  // 受信できたよ応答
  OSCMessage _msg("/got_it");
  _msg.add(ledState);
  Udp.beginPacket(TARGET_IP, TARGET_PORT);
  _msg.send(Udp);
  Udp.endPacket();
  _msg.empty();
}




void oscLoop() {

    
  // 受信処理
  // OSCBundleではなく、OSCMessage
  // rmsg.routeではなく、rmgs.dispatch
  OSCMessage rmsg;
  int size = Udp.parsePacket();
  if (size > 0) {
    while (size--) {
      rmsg.fill(Udp.read());
    }
    if (!rmsg.hasError()) {
      // アドレスごとに処理を振り分ける
      rmsg.dispatch("/HNG", dispatchHNG, 0);
      rmsg.empty();
    }
    else {
      error = rmsg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }

    
  }

 
//送信処理
 OSCMessage swtmsg("/C");
  swtmsg.add(smokeON);
    Udp.beginPacket(TARGET_IP, TARGET_PORT);
   
if(digitalRead(13) == HIGH){
  swtmsg.send(Udp);
  Udp.endPacket();
  delay(200);
} 

 
    swtmsg.empty();
    
  
  
}
