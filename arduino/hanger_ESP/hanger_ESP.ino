#include <SPI.h>

#include <deprecated.h>
#include <MFRC522.h>
#include <MFRC522Extended.h>



/**
 * ESP8266(ESP-WROOM-02)で、
 * OTAでネットワーク越しにファイルをアップデートできる仕組みにしつつ
 * UDPのデータの送受信を可能にするテスト用プログラム
 **/
#include <OSCBundle.h>
#include <OSCData.h>
#include <OSCMessage.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define RST_PIN 16
#define SS_PIN 2
#define COM_PIN 15
#define LED_PIN 0


const char* ssid = "nemunoki2G";
const char* password = "nemunoki2";

void setup() {
  Serial.begin(115200);

  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // switch off all the PWMs during upgrade
  ArduinoOTA.onStart([]() {
    Serial.println("start");
  });
  // do a fancy thing with our board led at end
  ArduinoOTA.onEnd([]() {
    Serial.println("end");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  // setup the OTA server
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  // setup UDP port
  oscSetup();

  mainSetup();
}

void loop() {
  ArduinoOTA.handle();
  oscLoop();
  mainLoop();
}
