

// 自分のポート番号
char myIP[16];
#define MY_OSC_PORT 8091

// 送り先IPアドレスとポート番号
const IPAddress TARGET_IP(192, 168, 11, 4);
#define TARGET_PORT 8090

WiFiUDP Udp;
OSCErrorCode error;


/**
 * 初期設定
 */
void oscSetup() {
  Udp.begin(MY_OSC_PORT);

  // 送信テスト(自分のIPアドレスを通知)
  {
    sprintf(myIP, "IP is %d.%d.%d.%d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3] );
    OSCMessage smsg("/ip");
    //OSCMessage swtmsg("/water");
    smsg.add(myIP);
    //swtmsg.add(smokeON);
    Udp.beginPacket(TARGET_IP, TARGET_PORT);
    smsg.send(Udp);
    Udp.endPacket();
    smsg.empty();
  }

  
}

/**
 * dispatchのテスト(B宛に来た場合のみ処理)
 */
void dispatchB(OSCMessage &msg) {
  int ledState = msg.getInt(0);
  if (ledState == 1) {
    sendIntData(12345); 
    delay(500);
    sendIntData(-12345);
  }
  else{
    
  }

  // 受信できたよ応答
  OSCMessage _msg("/got_it");
  _msg.add(ledState);
  Udp.beginPacket(TARGET_IP, TARGET_PORT);
  _msg.send(Udp);
  Udp.endPacket();
  _msg.empty();
}

/**
 * 繰り返し処理
 */
void oscLoop() {

  //OSCMessage swtmsg("/water");
    //swtmsg.add(smokeON);
    //Udp.beginPacket(TARGET_IP, TARGET_PORT);
    //int swtState = analogRead(13);

   /* if(analogRead(13) > 0){
      swtmsg.send(Udp);
      Udp.endPacket();
    
    }
    swtmsg.empty();
    */
    
  // 受信処理
  // ここがキモ(OSCBundleではなく、OSCMessageにする)
  // rmsg.routeではなく、rmgs.dispatchなとこも重要
  OSCMessage rmsg;
  int size = Udp.parsePacket();
  if (size > 0) {
    while (size--) {
      rmsg.fill(Udp.read());
    }
    if (!rmsg.hasError()) {
      // アドレスごとに処理を振り分ける
      rmsg.dispatch("/D", dispatchB, 0);
      //rmsg.dispatch("/led", dispatchLED, 0);
      //rmsg.dispatch("/hangerON", dispatchSwt, 0);
      rmsg.empty();
    }
    else {
      error = rmsg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }

    
  }
  
}

void sendIntData(int value) {
  Serial.write('H'); // ヘッダの送信
  Serial.write(lowByte(value)); // 下位バイトの送信
  Serial.write(highByte(value)); // 上位バイトの送信
}
