#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#define LED 6
#define NUM_LED 13

#define red  9
#define green 10
#define blue 11

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_LED, LED, NEO_GRB + NEO_KHZ800);

int relay = 4;
 
int recv_data; // 受信データ




void setup() {
  Serial.begin(115200);
  pixels.begin();
  pinMode(blue, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(relay, OUTPUT);
  digitalWrite(4, HIGH);
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif

  color(0,0,0,1);

}


void loop() { 

  // 受信バッファに３バイト（ヘッダ＋int）以上のデータが着ているか確認
  if ( Serial.available() >= sizeof('H') + sizeof(int) ) {
    // ヘッダの確認
    if ( Serial.read() == 'H' ) {
      int low = Serial.read(); // 下位バイトの読み取り
      int high = Serial.read(); // 上位バイトの読み取り
      recv_data = makeWord(high,low); // 上位バイトと下位バイトを合体させてint型データを復元
    }
  }

  // 受信したデータに基づいてLEDをON/OFF
  if ( recv_data == 12345 ) {
    digitalWrite(4, LOW);
    pixels.show();
    color(0,80,100,250);
  }
  if ( recv_data == -12345 ) {
    digitalWrite(4, HIGH);
    pixels.show();
    color(0,0,0,0);
    
  }
}

void color(int cred, int cgreen, int cblue, int brightness) {
  pixels.setBrightness(brightness);
  for(int i=0; i < NUM_LED; i++){
    pixels.setPixelColor(i,pixels.Color(cred,cgreen,cblue));
  }
}
